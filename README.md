Kinesitail
===========

A basic CLI tool that lets you watch a Kinesis stream for new records; when any new records appear, their contents are printed to stdout.

Usage
===========

Usage: `kinesis (-n|--stream-name ARG) [-e|--stream-endpoint ARG] [-p|--stream-port ARG]`


Read from a Kinesis Stream. If a stream URL is not specified, the environment variable `AWS_DEFAULT_REGION` is expected, as well as `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_SECURITY_TOKEN`

Available options:

| Flag | Description |
| ------ | ------ |
| -h, --help |  Show this help text
| -n, --stream-name ARG |     Kinesis Stream Name
| -e, --stream-endpoint ARG | Optional Kinesis Stream URL. Reads from Environment variables if not specified`
| -p, --stream-port ARG | Optional Kinesis Stream Port. Default: 4567



Installing
===========

1. Install [Haskell's Stack build tool](https://haskellstack.org)
    * On OSX This can be installed using [Homebrew](http://brew.sh/index.html), `brew install stack`
    * [Linux instructions](https://docs.haskellstack.org/en/stable/install_and_upgrade/)
2. Run `stack install`
3. Run `~/.local/bin/kinesis` (or add `~/.local/bin` to your path then run `kinesis`)

Known issues
============

* `$HOME/.aws/credentials` must exist in a valid format even when reading from a non-AWS stream.

Contributors
============

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

License
========

Copyright (c) 2016 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.
